<?php 
	
	include("cabecera.php");
	include("menu.php");
	echo '<div id="centro">';
	if (!$_POST['action'] == 'submitted') {   
	echo '<h2>Calculadora de puntos <a href="http://es.wikipedia.org/w/index.php?title=Heptatlón">Heptathlón</a>:</h2>';
	echo '<img src="galeria/Mechanical-Calculator.png" style="float:right; width:400px"/>';
	echo '<form action="heptathlon.php" method="post">
	<ul>
	  <li><dl><dt>Titulo de la prueba: </dt><dd><input type="text" name="titulo" size="50" value=""></input></dd></dl></li>
	  <li><dl><dt>Nombre atleta: </dt><dd><input type="text" name="nombre" size="50" value=""></input></dd></dl></li>
	  <li><dl><dt>Licencia: </dt><dd><input type="text" name="licencia" size="50" value=""></input></dd></dl></li>
	  <li><dl><dt>Fecha: </dt><dd><input type="text" name="fecha" size="50" value=""></input></dd></dl></li>
	  <li><dl><dt>Lugar: </dt><dd><input type="text" name="lugar" size="50" value=""></input></dd></dl></li>
	  <li><dl><dt>Club: </dt><dd><input type="text" name="club" size="50" value=""></input></dd></dl></li>
	</ul>
	<dl>
	<dt class="lista_datos"><abbr title="En segundos">60ml:</abbr></dt> <dd><input type="text" name="60ml" size="5" value="0">seg</input> <input type="text" name="60mlviento" size="5" value="0">met/seg</input></dd>
	<dt class="lista_datos"><abbr title="En metros">longitud:</abbr></dt> <dd><input type="text" name="longitud" size="5" value="0">met</input> <input type="text" name="longitudviento" size="5" value="0">met/seg</input></dd>
	<dt class="lista_datos"><abbr title="En metros">peso:</abbr></dt> <dd><input type="text" name="peso" size="5" value="0">met</input></dd>
	<dt class="lista_datos"><abbr title="En metros">altura:</abbr></dt> <dd><input type="text" name="altura" size="5" value="0">met</input></dd>
	<dt class="lista_datos"><abbr title="En segundos">60mv:</abbr></dt> <dd><input type="text" name="60mv" size="5" value="0">seg</input> <input type="text" name="60mvviento" size="5" value="0">met/seg</input></dd>
	<dt class="lista_datos"><abbr title="En metros">Pértiga:</abbr></dt> <dd><input type="text" name="pertiga" size="5" value="0">met</input></dd>
	<dt class="lista_datos"><abbr title="En minutos y en segundos">1000ml:</abbr></dt> <dd><input type="text" name="1000mlmin"  size="5" value="0">min </input>
	<input type="text" name="1000mlseg"  size="5" value="0">seg</input></dd>
	<input type="hidden" name="action" value="submitted"></input>
	<input type="hidden" name="modo" value="hepta"></input>
	</dd>
	<dl>
	<p><input type="submit" name="submit"></button>
	<input type="reset" name="reset"></button></p>
</form>';
	}
	else if ($_POST['modo'] == 'hepta') {
		$resultado = array("60ml" => 0, "longitud" => 0, "peso" => 0, "altura" => 0, "60mv" => 0, "pertiga" => 0, "1000ml" => 0);
		// 60 metros lisos
		$resultado['60ml'] = $_POST['60ml'];
		if ($resultado['60ml'] < 1) {
			$resultado['60ml'] = 11.4;
		}
		$resultado['60ml'] = floor(58.0150 * pow((11.50 - $resultado['60ml']),1.81));
		echo '<div class="bitacora" style="width: 25%;"><div class="asunto">60 metros lisos: </div><div class="noticia">';
		echo $resultado['60ml'];
		echo ' puntos</div>';
		// Longitud
		$resultado['longitud'] = $_POST['longitud'];
		if ($resultado['longitud'] < 2.2){
			$resultado['longitud'] = 2.2; 
		}
		$resultado['longitud'] = floor(0.14354 * pow((($resultado['longitud'] * 100) - 220),1.40));
  		echo '<div class="asunto">Longitud: </div><div class="noticia">';
		echo $resultado['longitud'];
		echo ' puntos</div>';
		// Peso
		$resultado['peso'] = $_POST['peso'];
  		if ($resultado['peso'] < 1.5) {
			$resultado['peso'] = 1.5;
		}
		$resultado['peso'] = floor(51.39 * pow((($resultado['peso']) - 1.5),1.05));
		echo '<div class="asunto">Peso: </div><div class="noticia">';
		echo $resultado['peso'];
		echo ' puntos</div>';
		// Altura
		$resultado['altura'] = $_POST['altura'];
   		if ($resultado['altura'] < 0.75) {
			$resultado['altura'] = 0.75;
		}
   		$resultado['altura'] = floor(0.8465* pow((($resultado['altura'] * 100) - 75.00),1.42));
		echo '<div class="asunto">Altura: </div><div class="noticia">';
		echo $resultado['altura'];
		echo ' puntos</div>';
		// 60 metros vallas
		$resultado['60mv'] =  $_POST['60mv'];
   		if ($resultado['60mv'] > 15.5) {
			$resultado['60mv'] = 15.5;
		}
   		if ($resultado['60mv'] <  1) {
			$resultado['60mv'] = 15.5;
		}
   		$resultado['60mv'] = floor(20.5173 * pow((15.50 - $resultado['60mv']),1.92));
		echo '<div class="asunto">60 metros vallas: </div><div class="noticia">';
		echo $resultado['60mv'];
		echo ' puntos</div>';
		// Pertiga
		$resultado['pertiga'] =  $_POST['pertiga'];
   		if ($resultado['pertiga'] < 1.02) {
			$resultado['pertiga'] = 1.02;
		}
   		$resultado['pertiga'] = floor(0.2797 * pow((($resultado['pertiga'] * 100) - 100.00),1.35));
		echo '<div class="asunto">Pértiga: </div><div class="noticia">';
		echo $resultado['pertiga'];
		echo ' puntos</div>';
		// 1000 ml
		$min = $_POST['1000mlmin'] * 1000;
		$seg = $_POST['1000mlseg'] * 1000;
  		if ($min < 1) { 
			$min = 5000;
   			$seg = 1760.00;	
		}
   		$min = ($min * 60);
   		$min = ($min + $seg);
   		$min = ($min / 1000);
   		$resultado['1000ml'] = floor(0.08713 * pow((305.50 - $min),1.85));
		echo '<div class="asunto">1000 metros: </div><div class="noticia">';
		echo $resultado['1000ml'];
		echo ' puntos</div></div>';
		$tmp = 0;
		foreach ($resultado as $valor) {
			$tmp += $valor;
		}
		$resultado['total'] = $tmp;
		echo '<hr/><div class="bitacora" style="width: 25%; float:right; clear:all"><div class="asunto">Resultado: </div><div class="noticia">';
		echo $resultado['total'];
		echo ' puntos</div></div>';

		$resultado_string = implode("|", $resultado);
		$viento_string = $_POST['60mlviento'].'|'.$_POST['longitudviento'].'|'.$_POST['60mvviento'];
		$tiempo_string = $_POST['60ml'].'|'.$_POST['longitud'].'|'.$_POST['peso'].'|'.$_POST['altura'].'|'.$_POST['60mv'].'|'.$_POST['pertiga'].'|'.$_POST['1000mlmin'].':'.$_POST['1000mlseg'];
		echo '<div style="clear:both;"></div><p class="comentario">
		<a href="heptathlonpdf.php?titulo='.$_POST['titulo'].'&nombre='.$_POST['nombre'].'&licencia='.$_POST['licencia'].'&fecha='.$_POST['fecha'].'&lugar='.$_POST['lugar'].'&club='.$_POST['club'].'&resultado='.$resultado_string.'&viento='.$viento_string.'&tiempo='.$tiempo_string.'">
		Descargar informe completo en pdf</a></p>';


	}
	
	include("pie.php");
?>
