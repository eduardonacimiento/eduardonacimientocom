<?php
$titulo = $_GET['titulo'];
$texto = $_GET['texto'];
include ('class.ezpdf.php');
$pdf = new Cezpdf();
//$pdf->selectFont('fonts/Helvetica.afm');
$diff=array(196=>'Adieresis',228=>'adieresis',
            214=>'Odieresis',246=>'odieresis',
            220=>'Udieresis',252=>'udieresis',
            223=>'germandbls');
// and the first time that you call selectFont for each font, use
$pdf->selectFont('fonts/Helvetica.afm',array('encoding'=>'WinAnsiEncoding','differences'=>$diff));


$datacreator = array (
         'Title'=>'Resultado Pruebas Combinadas',
         'Author'=>'Eduardo Nacimiento Garcia',
         'Subject'=>'Resultado pruebas combinadas',
         'Creator'=>'micorreo@eduardonacimiento.com',
	 'Producer'=>'http://www.eduardonacimiento.com'
);
$pdf->addInfo($datacreator);


$pdf->ezText('<c:alink:http://www.eduardonacimiento.com>www.eduardonacimiento.com</c:alink>', 10,array('justification'=>'right') );
$pdf->ezText('');
$pdf->ezText('<u>'.$titulo.'</u>', 15, array('justification'=>'center'));
$pdf->ezText('');
$pdf->ezText($nombre, 12);
$pdf->ezText($licencia, 12);
$pdf->ezText($fecha, 12);
$pdf->ezText($lugar, 12);
$pdf->ezText($club, 12);
$pdf->ezText('');
$pdf->ezText('');
$resultado = explode("|", $_GET['resultado']);
$viento = explode("|", $_GET['viento']);
$tiempo = explode("|", $_GET['tiempo']);
$datos = array(
 array('num'=>1,'Prueba'=>'60 ml','Resultado'=>$tiempo[0], 'Viento'=>$viento[0],'Puntos'=>$resultado[0],)
,array('num'=>2,'Prueba'=>'Longitud','Resultado'=>$tiempo[1],'Viento'=>$viento[1],'Puntos'=>$resultado[1],)
,array('num'=>3,'Prueba'=>'Peso','Resultado'=>$tiempo[2],'Puntos'=>$resultado[2],)
,array('num'=>4,'Prueba'=>'Altura','Resultado'=>$tiempo[3],'Puntos'=>$resultado[3],)
,array('num'=>5,'Prueba'=>'60 mv','Resultado'=>$tiempo[4],'Viento'=>$viento[2],'Puntos'=>$resultado[4],)
,array('num'=>6,'Prueba'=>'Pertiga','Resultado'=>$tiempo[5],'Puntos'=>$resultado[5],)
,array('num'=>7,'Prueba'=>'1000 ml','Resultado'=>$tiempo[6],'Puntos'=>$resultado[6],)
,array('num'=>'')
,array('Prueba'=>'Total','Puntos'=>$resultado[7],)
);

$pdf->ezTable($datos);


$pdf->ezStream();


?>


