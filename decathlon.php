<?php 
	
	include("cabecera.php");
	include("menu.php");
	echo '<div id="centro">';
	if (!$_POST['action'] == 'submitted') {   
	echo '<h2>Calculadora de puntos <a href="http://es.wikipedia.org/w/index.php?title=Decatlón">Decathlón</a>:</h2>';
	echo '<img src="galeria/Mechanical-Calculator.png" style="float:right; width:400px"/>';
	echo '<form action="decathlon.php" method="post">
	<ul>
	  <li><dl><dt>Titulo de la prueba: </dt><dd><input type="text" name="titulo" size="50" value=""></input></dd></dl></li>
	  <li><dl><dt>Nombre atleta: </dt><dd><input type="text" name="nombre" size="50" value=""></input></dd></dl></li>
	  <li><dl><dt>Licencia: </dt><dd><input type="text" name="licencia" size="50" value=""></input></dd></dl></li>
	  <li><dl><dt>Fecha: </dt><dd><input type="text" name="fecha" size="50" value=""></input></dd></dl></li>
	  <li><dl><dt>Lugar: </dt><dd><input type="text" name="lugar" size="50" value=""></input></dd></dl></li>
	  <li><dl><dt>Club: </dt><dd><input type="text" name="club" size="50" value=""></input></dd></dl></li>
	</ul>
	<dl>
	<dt class="lista_datos"><abbr title="En segundos">100ml:</abbr></dt> <dd><input type="text" name="100ml" size="5" value="0">seg</input> <input type="text" name="100mlviento" size="5" value="0">met/seg</input></dd>
	<dt class="lista_datos"><abbr title="En metros">longitud:</abbr></dt> <dd><input type="text" name="longitud" size="5" value="0">met</input> <input type="text" name="longitudviento" size="5" value="0">met/seg</input></dd>
	<dt class="lista_datos"><abbr title="En metros">peso:</abbr></dt> <dd><input type="text" name="peso" size="5" value="0">met</input></dd>
	<dt class="lista_datos"><abbr title="En metros">altura:</abbr></dt> <dd><input type="text" name="altura" size="5" value="0">met</input></dd>
	<dt class="lista_datos"><abbr title="En metros">400ml:</abbr></dt> <dd><input type="text" name="400ml" size="5" value="0">seg</input></dd>
	<dt class="lista_datos"><abbr title="En segundos">110mv:</abbr></dt> <dd><input type="text" name="110mv" size="5" value="0">seg</input> <input type="text" name="110mvviento" size="5" value="0">met/seg</input></dd>
	<dt class="lista_datos"><abbr title="En metros">Disco:</abbr></dt> <dd><input type="text" name="disco" size="5" value="0">met</input></dd>
	<dt class="lista_datos"><abbr title="En metros">Jabalina:</abbr></dt> <dd><input type="text" name="jabalina" size="5" value="0">met</input></dd>
	<dt class="lista_datos"><abbr title="En metros">Pértiga:</abbr></dt> <dd><input type="text" name="pertiga" size="5" value="0">met</input></dd>
	<dt class="lista_datos"><abbr title="En minutos y en segundos">1500ml:</abbr></dt> <dd><input type="text" name="1500mlmin"  size="5" value="0">min </input>
	<input type="text" name="1500mlseg"  size="5" value="0">seg</input></dd>
	<input type="hidden" name="action" value="submitted"></input>
	<input type="hidden" name="modo" value="deca"></input>
	</dd>
	<dl>
	<p><input type="submit" name="submit"></button>
	<input type="reset" name="reset"></button></p>
</form>';
	}
	else if ($_POST['modo'] == 'deca') {
		$resultado = array("100ml" => 0, "longitud" => 0, "peso" => 0, "altura" => 0, "400ml" => 0, "110mv" => 0, "disco" => 0, "pertiga" => 0, "jabalina" => 0, "1500ml" => 0);
		// 100 ml
		$resultado['100ml'] = $_POST['100ml'];
		if ($resultado['100ml'] != 0) {
			$resultado['100ml'] = floor(25.437 * pow((18.0 - $resultado['100ml']),1.81));
		}
		echo '<div class="bitacora" style="width: 25%;"><div class="asunto">100 metros lisos: </div><div class="noticia">';
		echo $resultado['100ml'];
		echo ' puntos</div>';
		// longitud
		$resultado['longitud'] = $_POST['longitud'] * 100;
		if ($resultado['longitud'] != 0) {
			$resultado['longitud'] = floor(0.14354 * pow(($resultado['longitud'] - 220),1.40));
		}
		echo '<div class="asunto">Longitud: </div><div class="noticia">';
		echo $resultado['longitud'];
		echo ' puntos</div>';
		// peso
		$resultado['peso'] = $_POST['peso'];
		if ($resultado['peso'] != 0) {
			$resultado['peso'] = floor(51.39 * pow(($resultado['peso'] - 1.5),1.05));
		}
		echo '<div class="asunto">Peso: </div><div class="noticia">';
		echo $resultado['peso'];
		echo ' puntos</div>';
		// altura
		$resultado['altura'] = $_POST['altura'] * 100;
		if ($resultado['altura'] != 0) {
			$resultado['altura'] = floor(0.8465 * pow(($resultado['altura'] - 75),1.42));
		}
		echo '<div class="asunto">Altura: </div><div class="noticia">';
		echo $resultado['altura'];
		echo ' puntos</div>';
		// 400 ml
		$resultado['400ml'] = $_POST['400ml'];
		if ($resultado['400ml'] != 0) {
			$resultado['400ml'] = floor(1.53775 * pow((82 - $resultado['400ml']),1.81));
		}
		echo '<div class="asunto">400 metros lisos: </div><div class="noticia">';
		echo $resultado['400ml'];
		echo ' puntos</div>';
		// 110 mv
		$resultado['110mv'] = $_POST['110mv'];
		if ($resultado['110mv'] != 0) {
			$resultado['110mv'] = floor(5.74352 * pow((28.5 - $resultado['110mv']),1.92));
		}
		echo '<div class="asunto">110 metros vallas: </div><div class="noticia">';
		echo $resultado['110mv'];
		echo ' puntos</div>';
		// disco
		$resultado['disco'] = $_POST['disco'];
		if ($resultado['disco'] != 0) {
			$resultado['disco'] = floor(12.91 * pow(($resultado['disco'] - 4.0),1.1));
		}
		echo '<div class="asunto">Dsico: </div><div class="noticia">';
		echo $resultado['disco'];
		echo ' puntos</div>';
		// pertiga
		$resultado['pertiga'] = $_POST['pertiga'] * 100;
		if ($resultado['pertiga'] != 0) {
			$resultado['pertiga'] = floor(0.2797 * pow(($resultado['pertiga'] - 100),1.35));
		}
		echo '<div class="asunto">Pertiga: </div><div class="noticia">';
		echo $resultado['pertiga'];
		echo ' puntos</div>';
		// jabalina
		$resultado['jabalina'] = $_POST['jabalina'];
		if ($resultado['jabalina'] != 0) {
			$resultado['jabalina'] = floor(10.14 * pow(($resultado['jabalina'] - 7.0),1.08));
		}
		echo '<div class="asunto">Jabalina: </div><div class="noticia">';
		echo $resultado['jabalina'];
		echo ' puntos</div>';
		// 1500 ml
		$resultado['1500ml'] = $_POST['1500mlmin'] * 60;
		$resultado['1500ml'] += $_POST['1500mlseg'];
		if ($resultado['1500ml'] != 0) {
			$resultado['1500ml'] = floor(0.03768 * pow((480 - $resultado['1500ml']),1.85));
		}
		echo '<div class="asunto">1500 metros lisos: </div><div class="noticia">';
		echo $resultado['1500ml'];
		echo ' puntos</div>';
		echo '</div>';
		$tmp = 0;
		foreach ($resultado as $valor) {
			$tmp += $valor;
		}
		$resultado['total'] = $tmp;
		echo '<hr/><div class="bitacora" style="width: 25%; float:right; clear:all"><div class="asunto">Resultado: </div><div class="noticia">';
		echo $resultado['total'];
		echo ' puntos</div></div>';

		$resultado_string = implode("|", $resultado);
		$viento_string = $_POST['100mlviento'].'|'.$_POST['longitudviento'].'|'.$_POST['110mvviento'];
		$tiempo_string = $_POST['100ml'].'|'.$_POST['longitud'].'|'.$_POST['peso'].'|'.$_POST['altura'].'|'.$_POST['400ml'].'|'.$_POST['110mv'].'|'.$_POST['disco'].'|'.$_POST['pertiga'].'|'.$_POST['jabalina'].'|'.$_POST['1500mlmin'].':'.$_POST['1500mlseg'];
		echo '<div style="clear:both;"></div><p class="comentario">
		<a href="decathlonpdf.php?titulo='.$_POST['titulo'].'&nombre='.$_POST['nombre'].'&licencia='.$_POST['licencia'].'&fecha='.$_POST['fecha'].'&lugar='.$_POST['lugar'].'&club='.$_POST['club'].'&resultado='.$resultado_string.'&viento='.$viento_string.'&tiempo='.$tiempo_string.'">
		Descargar informe completo en pdf</a></p>';


	}
	
	include("pie.php");
?>
