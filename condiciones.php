<?php 
	include("cabecera.php");
	include("menu.php");
	include("publicidad.php");
	echo '<div id="centro">';	

	include("config.php");
	
	echo '<div class="asunto">Condiciones de uso:</div>
	<div class="noticia">
		<p>Si te registras y/o publicas alguna foto o comentario o tema del foro, significa que aceptas las siguientes condiones de uso.</p>
		<dl>
			<dt><h4>Licencia GNU FDL:</h4></dt>
			<dd>Todo contenido que sea publicado en esta web se encuentra licenciado bajo las condiciones de la licencia GNU FDL, que se encuantra detallada en este <a href="http://www.gnu.org/copyleft/fdl.html">enlace</a>.</dd>

			<dt><h4>Contenidos apropiados:</h4></dt>
			<dd>Todo contenido que sea publicado en esta web debe cumplir ciertos criterios que el administrador considere apropiados, y que en caso contrario podrá modificar y/o eliminar.</dd>

			<dt><h4>Responsabilidad:</h4></dt>
			<dd>Aunque el administrador puede revisar el contenido de las publicaciones de otros usuario, es cada usuario el único responsable de sus publicaciones, sin que de ninguna manera se pudieran reclamar resposabilidades al administrador.</dd>

			<dt><h4>Otras:</h4></dt>
			<dd>En caso de que surja alguna otra cuestión no contemplada en estas condiciones, comuníquelo al administrador mediante el formulario de <a href="contacto.php">contacto</a> de esta web.</dd>
		</dl>
	</div>';

	include("pie.php");
?>
