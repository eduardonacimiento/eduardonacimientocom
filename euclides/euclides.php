<?php
function euclides ($a, $b, $inversoant)
{
	$dividendo = $b;
	$divisor = $a;
	$inverso = 1;
	$inversoant = 0;
	$resto = 1;
	while ($resto > 0) {
     		$cociente = (int) ($dividendo / $divisor);
	  	$resto = ($dividendo % $divisor);
     		$inversopos = ((-$cociente) * $inverso) + $inversoant;
     		$dividendo = $divisor;
     		$divisor = $resto;
     		$inversoant = $inverso;
     		$inverso = $inversopos;
	}
	if ($inversoant < 0) {
		$inversoant += $b;
	}
	return $dividendo;
}
?> 
